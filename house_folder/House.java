


public class House{
  /*
  Syntax of defining memebers of the java class is,
    <modifier> type <name>;
  */
  private Window win;
  /*
  Syntax of defining methods of the java class is,
  <modifier> <return-type> methodName(<optional-parameter-list>) <exception-list>{
                    ...
  }
  */

  public House(){
  
  
	  this.win = new Window();
	  this.win.setType("Wood");
  }

  public void setWin(Window win){
    //set passed parameter as name
    this.win = win;
  }
  public Window getWin(){
    //return the set name
    return this.win;
  }

  public void printWindowType(){
  
    System.out.println(this.win.getType());
  }

}


