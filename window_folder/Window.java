



public class Window{
  /*
  Syntax of defining memebers of the java class is,
    <modifier> type <name>;
  */
  private String type;
  /*
  Syntax of defining methods of the java class is,
  <modifier> <return-type> methodName(<optional-parameter-list>) <exception-list>{
                    ...
  }
  */
  public void setType(String type){
    //set passed parameter as name
    this.type = type;
  }
  public String getType(){
    //return the set name
    return this.type;
  }

}




